113
00:06:18,600 --> 00:06:22,410
soweit claudio mitorganisator von StrikeWEF

114
00:06:23,610 --> 00:06:27,270
Claudio hat das einbeziehen der lokalen Bevölkerung

115
00:06:27,270 --> 00:06:30,300
in diesen Protest erwähnt das ist für

116
00:06:30,300 --> 00:06:33,420
die Akzeptanz des Protests wichtig und ist

117
00:06:33,420 --> 00:06:37,170
in Davos besonders relevant weil die Einwohner

118
00:06:37,170 --> 00:06:41,160
dort eher für das Weltwirtschaftsforum sind aus

119
00:06:41,160 --> 00:06:43,680
wirtschaftlichen Gründen denn es winken hohe

120
00:06:43,680 --> 00:06:48,000
Einnahmen durch Hotelgäste und der Bekanntheitsgrad

121
00:06:48,000 --> 00:06:49,740
der Gegend lockt Touristen an

122
00:06:50,940 --> 00:06:54,420
um uns die Dimensionen dieser Veranstaltung klarzumachen

123
00:06:54,480 --> 00:06:58,560
das Weltwirtschaftsforum ist eine Stiftung und Lobbyorganisation

124
00:06:58,560 --> 00:07:03,720
mit etwa 1000 Mitgliedsunternehmen zur Veranstaltung kommen

125
00:07:03,720 --> 00:07:07,860
etwa 500 Journalisten 5 Tage lang dazu.

126
00:07:07,860 --> 00:07:11,040
In der gleichen Größenordnung Politiker und Wissenschaftler

127
00:07:11,040 --> 00:07:14,220
und dann auch die Polizei und Armee

128
00:07:14,220 --> 00:07:18,900
zur absicherung und zwar einige tausend Polizisten

129
00:07:18,900 --> 00:07:23,400
und Soldaten zu den Mitgliedern des Weltwirtschaftsforums

130
00:07:23,790 --> 00:07:26,820
gehören die weltweit größten Konzerne zum Beispiel

131
00:07:26,820 --> 00:07:29,610
auch die Deutschebank Tochter 

132
00:07:29,610 --> 00:07:31,193
DWS grob hat

133
00:07:31,200 --> 00:07:34,710
jüngst wieder für negative Schlagzeilen gesorgt angefangen

134
00:07:34,710 --> 00:07:37,830
hat es vor zwei jahren als eine

135
00:07:37,830 --> 00:07:41,940
Nachhaltigkeitsmanagements ihnen aufdeckte dass die siphon gesellschaft

136
00:07:42,120 --> 00:07:45,840
ihre fotoprodukte grüne bewarb als sie tatsächlich

137
00:07:45,840 --> 00:07:49,950
waren seitdem ermitteln u s amerikanische und

138
00:07:49,950 --> 00:07:51,360
deutsche aufsichtsbehörden

139
00:07:51,390 --> 00:07:53,700
gegen die d w s und gegen

140
00:07:53,700 --> 00:07:58,110
die konzernmutter deutsche bank wegen kapitalanlagebetrug im

141
00:07:58,110 --> 00:08:01,980
zusammenhang mit greenwashing in der zwischenzeit konnte

142
00:08:01,980 --> 00:08:05,310
greenpeace in mehreren studien der deutschen bank

143
00:08:05,310 --> 00:08:10,710
tochter einige fälle von greenwashing nachweisen in

144
00:08:10,710 --> 00:08:11,520
der neuesten

145
00:08:11,520 --> 00:08:14,970
die studie geht es um die gehaltsstrukturen

146
00:08:14,970 --> 00:08:19,500
von topmanagern die greenwashing vorwort um bonuszahlungen

147
00:08:19,500 --> 00:08:22,920
zu erreichen reichte scheinbar aus fonds als

148
00:08:22,920 --> 00:08:25,320
nachhaltig zu etikettieren auch wenn sie es

149
00:08:25,320 --> 00:08:28,080
nicht sind greenpeace fordert von der b

150
00:08:28,080 --> 00:08:31,650
w s eine vergütungsstruktur die die verhältnismäßigkeit

151
00:08:31,680 --> 00:08:34,289
von gehältern ward und die relevanz von

152
00:08:34,289 --> 00:08:37,620
umwelt zielen sicherstellt umweltziele wie etwa die

153
00:08:37,620 --> 00:08:41,850
einhaltung des pariser klimaschutzabkommens die deutsche bank

154
00:08:42,120 --> 00:08:45,240
als mehrheit eigentümerin der d w s

155
00:08:45,300 --> 00:08:47,970
trägt eine besondere verantwortung für die greenwashing

156
00:08:47,970 --> 00:08:50,310
skandale und ist in der pflicht die

157
00:08:50,310 --> 00:08:51,840
genannten missstände schnell

158
00:08:51,849 --> 00:08:53,190
ist möglich zu beheben

159
00:08:53,970 --> 00:08:57,060
um etwas druck aufzubauen gibt es eine

160
00:08:57,390 --> 00:09:02,370
mitmach-aktion von greenpeace und zwar heiß die

161
00:09:02,370 --> 00:09:07,200
kalender fluten der kalender des gps chefs

162
00:09:07,200 --> 00:09:10,500
soll geflutet werden wir schicken einladungen an

163
00:09:10,830 --> 00:09:13,140
den ddr schiff um ihn an seine

164
00:09:13,140 --> 00:09:13,830
klimaforscher

165
00:09:13,920 --> 00:09:16,590
rechen zu erinnern wie das ganze geht

166
00:09:16,650 --> 00:09:20,100
steht auf der greenpeace deutschland homepage und

167
00:09:20,100 --> 00:09:23,490
die studie kann auch dort nachgelesen werden

