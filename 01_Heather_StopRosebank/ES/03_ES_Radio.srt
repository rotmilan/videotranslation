95
00:04:14,250 --> 00:04:17,610
En resumen, Rosebank puede

96
00:04:17,640 --> 00:04:20,370
no ser explotado, porque queremos que sea

97
00:04:20,370 --> 00:04:23,789
simplemente no puede permitirse más en vista del calentamiento global

98
00:04:23,840 --> 00:04:26,090
también puede seguir transportando este CO2 al

99
00:04:26,090 --> 00:04:29,130
al aire. Pero Gran Bretaña, el gobierno

100
00:04:29,130 --> 00:04:32,520
de Gran Bretaña es incluso la promoción de este

101
00:04:32,520 --> 00:04:34,150
yacimiento de petróleo y gas. Además

102
00:04:34,200 --> 00:04:37,200
que Rosebank es en realidad una zona protegida

103
00:04:37,200 --> 00:04:40,440
región y es una

104
00:04:40,440 --> 00:04:44,190
corredor migratorio para las ballenas. Quien lo solicite

105
00:04:44,190 --> 00:04:46,680
quiere firmar las peticiones que mencionó Heather,

106
00:04:46,680 --> 00:04:52,440
en la página web 99-percent.org

107
00:04:52,440 --> 00:04:54,360
encontrará más información.

108
00:04:54,360 --> 00:04:58,320
No sólo Equinor y el

109
00:04:58,320 --> 00:05:02,010
Gobierno del Reino Unido quieren crear en el Mar del Norte

110
00:05:02,310 --> 00:05:05,760
petróleo, Shell también sigue con

111
00:05:05,760 --> 00:05:09,990
y Greenpeace ha protestado contra ella.

112
00:05:10,110 --> 00:05:13,350
y es que los activistas han

113
00:05:13,469 --> 00:05:14,520
plataforma de producción

114
00:05:14,520 --> 00:05:17,700
enganchada en el Mar del Norte

115
00:05:17,700 --> 00:05:21,087
fue transportado. A modo de antecedentes, Shell ha

116
00:05:21,087 --> 00:05:24,990
obtuvo un beneficio récord el año pasado,

117
00:05:24,990 --> 00:05:28,110
debido a la subida de los precios del petróleo tras la guerra de Ucrania.

118
00:05:28,200 --> 00:05:32,160
El beneficio ajustado se duplicó

119
00:05:32,490 --> 00:05:34,140
hasta los 36.000 millones de euros.

120
00:05:34,400 --> 00:05:37,520
Así que Shell es un ganador de guerra. Así son las cosas ahora,

121
00:05:37,520 --> 00:05:40,000
que la empresa siga invirtiendo en actividades que dañan el clima

122
00:05:39,980 --> 00:05:44,740
combustibles fósiles, ¡es una irresponsabilidad!

123
00:05:45,300 --> 00:05:48,539
Silja Zimmermann, activista escaladora de Greenpeace Alemania,

124
00:05:48,810 --> 00:05:51,210
estuvo presente en la acción de protesta en el Mar del Norte.

125
00:05:51,300 --> 00:05:54,360
Exijo a Shell este proyecto inmediatamente

126
00:05:54,360 --> 00:05:54,840
y que se retire de nuestro

127
00:05:55,440 --> 00:05:58,710
gobiernos por fin exijo responsabilidades

128
00:05:58,740 --> 00:06:01,860
y en su lugar invertir significativamente en

129
00:06:01,860 --> 00:06:05,700
energías renovables. Con esta plataforma de producción

130
00:06:05,700 --> 00:06:09,330
Shell planea 8 pozos más en el Mar del Norte

131
00:06:09,330 --> 00:06:13,380
Campo petrolífero Penguins. Es el primer

132
00:06:13,440 --> 00:06:14,789
nuevo tripulado

133
00:06:15,030 --> 00:06:17,760
Plataforma de producción de petróleo para Shell en el norte

134
00:06:17,760 --> 00:06:20,730
Mar del Norte durante 30 años. Este proyecto es

135
00:06:20,730 --> 00:06:22,330
fuera de plazo. Si ahora

136
00:06:22,320 --> 00:06:23,440
fuera de tiempo. Si ahora

137
00:06:23,310 --> 00:06:23,430
ya en Shell, todavía puedo

138
00:06:23,440 --> 00:06:24,810
Ya estoy en Shell, todavía puedo

139
00:06:24,810 --> 00:06:26,669
buenas noticias

140
00:06:27,270 --> 00:06:31,380
Shell tiene que pagar por la contaminación, 15 millones de euros.

141
00:06:31,409 --> 00:06:35,970
Cuatro granjeros nigerianos y sus comunidades

142
00:06:36,780 --> 00:06:39,720
la petrolera Shell tiene que pagar

143
00:06:39,750 --> 00:06:43,679
15 millones de euros en indemnizaciones. Derribados oleoductos de Shell

144
00:06:43,679 --> 00:06:46,350
habían contaminado sus tierras.

145
00:06:46,350 --> 00:06:47,400
La asociación de socios BUND,

146
00:06:47,429 --> 00:06:51,659
Amigos de la Tierra, apoyó a los afectados.

147
00:06:51,659 --> 00:06:54,450
Eso fue en 2008, Shell fue

148
00:06:54,900 --> 00:06:58,530
acusada en la sede de la compañía en La Haya.

149
00:06:58,530 --> 00:07:02,580
Mientras tanto, se ha llegado al veredicto, pero

150
00:07:02,580 --> 00:07:06,030
las petroleras han tomado más pueblos en el

151
00:07:06,030 --> 00:07:07,560
Delta del Níger

152
00:07:07,560 --> 00:07:09,570
privados de sus medios de subsistencia.

153
00:07:10,409 --> 00:07:13,860
Al menos la indemnización establece un nuevo estándar.

154
00:07:14,400 --> 00:07:17,515
Lo dice Donald Pols, de Amigos de la Tierra Europa Holanda.

155
00:07:17,520 --> 00:07:18,520
En el futuro será más fácil

156
00:07:18,520 --> 00:07:20,820
En el futuro será más fácil

157
00:07:20,820 --> 00:07:23,580
las empresas a rendir cuentas.

158
00:07:24,040 --> 00:07:26,860
Ahora hay que contrastar estos 15 millones

159
00:07:26,940 --> 00:07:31,830
con los 36 mil millones de beneficio,

160
00:07:31,830 --> 00:07:37,049
que Shell obtuvo el año pasado.

161
00:07:37,409 --> 00:07:40,320
Así que los órdenes de magnitud de los que estamos hablando,

162
00:07:40,320 --> 00:07:44,070
0,5 por ciento de los beneficios que Shell tiene que

163
00:07:44,669 --> 00:07:48,240
en concepto de indemnización.

164
00:07:48,240 --> 00:07:51,390
Así que sale de la caja chica se puede decir.

165
00:07:51,392 --> 00:07:53,400
No obstante, marquemos esto como una buena noticia.