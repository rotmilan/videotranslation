1
00:00:00,090 --> 00:00:02,690
Este es el mostrador de información de Regeneradio,

2
00:00:02,719 --> 00:00:04,920
la radio Greenpeace,

3
00:00:04,930 --> 00:00:06,810
en la radio libre de Stuttgart.

4
00:00:06,890 --> 00:00:09,860
La protesta climática en Lützerath,

5
00:00:09,860 --> 00:00:13,820
tiene mucha atención internacional

6
00:00:13,820 --> 00:00:15,890
atención. Esa es también una razón

7
00:00:15,920 --> 00:00:19,840
por la que la manifestación del 11.02 en Stuttgart fue internacional.

8
00:00:19,880 --> 00:00:22,310
También vemos que la interconexión de la

9
00:00:22,310 --> 00:00:26,390
movimiento de protección del clima está progresando en todo el mundo.

10
00:00:26,390 --> 00:00:29,540
Así que no es de extrañar que

11
00:00:29,540 --> 00:00:32,750
tenga un discurso en inglés.

12
00:00:32,750 --> 00:00:37,370
Subimos a Heather de la organización 99-percent.org.

13
00:00:37,519 --> 00:00:39,919
La dirección web es www.99-percent.org.

14
00:00:39,980 --> 00:00:43,459
Es una organización con

15
00:00:43,459 --> 00:00:47,390
tiene su origen en el Reino Unido. Hace algo sobre

16
00:00:47,390 --> 00:00:53,000
nuestra economía actual bajo el capitalismo.

17
00:00:53,000 --> 00:00:55,790
Empobreciendo al 99 por ciento de la población

18
00:00:55,794 --> 00:00:58,849
y al 1 por ciento extremadamente más rico.

19
00:00:58,879 --> 00:01:00,080
Habla de su campaña actual.

20
00:01:00,110 --> 00:01:02,209
En primer lugar quiero decir que

21
00:01:02,209 --> 00:01:04,550
se trata de Rosebank y

22
00:01:04,580 --> 00:01:07,070
un yacimiento de petróleo y gas aún sin explotar

23
00:01:07,070 --> 00:01:09,920
en el Mar del Norte. La empresa Equinor quiere

24
00:01:10,040 --> 00:01:11,420
explotar eso ahora.

25
00:01:13,400 --> 00:01:17,120
Hola Stuttgart y gracias

26
00:01:17,120 --> 00:01:19,220
StrikeWEF y Milan en StrikeWEF por esto

27
00:01:19,220 --> 00:01:21,259
oportunidad de hablar. Mi nombre es Heather

28
00:01:21,380 --> 00:01:23,480
y hoy les hablo desde

29
00:01:23,480 --> 00:01:25,250
el Reino Unido. Estoy aquí para hablar

30
00:01:25,250 --> 00:01:27,530
sobre una campaña que se está llevando a cabo en el

31
00:01:27,530 --> 00:01:30,650
una campaña medioambiental muy importante llamada

32
00:01:30,920 --> 00:01:31,700
Stop Rosebank.

33
00:01:31,710 --> 00:01:33,750
Déjame hablarte de Rosebank.

34
00:01:33,760 --> 00:01:36,680
Rosebank es el mayor yacimiento de petróleo y gas sin explotar de

35
00:01:36,680 --> 00:01:38,570
el mar del norte y el mar del norte

36
00:01:38,570 --> 00:01:41,240
está al norte de escocia. Por qué

37
00:01:41,240 --> 00:01:43,070
¿es para tanto?

38
00:01:43,100 --> 00:01:44,900
Es un gran problema porque Rosebank es

39
00:01:44,990 --> 00:01:47,090
enorme. Es tres veces el tamaño de

40
00:01:47,090 --> 00:01:49,580
el yacimiento petrolífero de Cambo y puede que hayas oído

41
00:01:49,580 --> 00:01:51,770
de Cambo porque en el último par

42
00:01:51,770 --> 00:01:52,039
de años

43
00:01:52,039 --> 00:01:55,729
los activistas y manifestantes han conseguido

44
00:01:55,729 --> 00:02:00,140
conseguir que Shell se retire de Cambo. Así que

45
00:02:00,160 --> 00:02:03,080
si quemáramos todo el

46
00:02:03,080 --> 00:02:05,300
el petróleo y el gas de Rosebank,

47
00:02:05,320 --> 00:02:07,759
produciría tanto CO2 -

48
00:02:07,779 --> 00:02:10,699
más CO2 que los 28 más bajos

49
00:02:10,699 --> 00:02:12,200
ingresos del mundo.

50
00:02:12,200 --> 00:02:14,060
Así que permítanme decir que

51
00:02:14,120 --> 00:02:17,240
de nuevo - si quemamos todo el petróleo en Rosebank

52
00:02:17,240 --> 00:02:18,740
la cantidad de CO2 es

53
00:02:18,740 --> 00:02:21,380
equivalente a la producida por el

54
00:02:21,410 --> 00:02:23,780
700 millones de personas que viven en esos

55
00:02:23,780 --> 00:02:25,940
28 países.

56
00:02:26,850 --> 00:02:28,490
Entonces, ¿por qué te hablo de

57
00:02:28,490 --> 00:02:31,640
¿esto ahora? Porque es urgente.

58
00:02:31,640 --> 00:02:32,799
Una empresa llamada Equinor,

59
00:02:33,230 --> 00:02:37,670
que es la compañía petrolera estatal de Noruega, tiene

60
00:02:37,670 --> 00:02:39,670
solicitado al Gobierno del Reino Unido el desarrollo de Rosebank

61
00:02:39,691 --> 00:02:44,600
y la decisión es inminente por lo que

62
00:02:46,040 --> 00:02:47,630
es muy importante que todos apoyemos

63
00:02:47,630 --> 00:02:50,329
esta campaña. ¿Por qué es tan

64
00:02:50,329 --> 00:02:52,490
algo malo? ¿Sabes por qué es

65
00:02:52,490 --> 00:02:53,990
algo malo. Todos sabemos que el

66
00:02:53,990 --> 00:02:56,030
ciencia. Todos sabemos que

67
00:02:56,030 --> 00:02:58,670
tenemos que detener todo nuevo petróleo y

68
00:02:58,670 --> 00:03:01,550
proyectos de gas. Y es algo realmente malo

69
00:03:01,550 --> 00:03:03,110
también para la ecología.

70
00:03:03,110 --> 00:03:06,110
El yacimiento petrolífero de Rosebank está en el

71
00:03:06,130 --> 00:03:09,890
corredor migratorio de cachalotes y rorcuales comunes

72
00:03:09,960 --> 00:03:13,280
y también es un área ecológicamente protegida

73
00:03:13,270 --> 00:03:17,510
debido al cinturón de esponjas Shetland-Faroe

74
00:03:17,540 --> 00:03:20,420
También el gobierno del Reino Unido

75
00:03:20,420 --> 00:03:22,609
absolutamente cómplice de esto porque son

76
00:03:22,790 --> 00:03:26,120
subvencionando esta exploración petrolífera

77
00:03:26,120 --> 00:03:28,820
masivamente por valor de

78
00:03:28,850 --> 00:03:31,530
500 millones de libras de los contribuyentes británicos

79
00:03:31,560 --> 00:03:34,000
que no verán nada de ello.

80
00:03:33,990 --> 00:03:36,590
De todos modos, ¿qué puedes hacer para ayudar?

81
00:03:36,590 --> 00:03:38,090
Mi llamada a la acción para ti

82
00:03:38,090 --> 00:03:41,720
hoy es esta. Es ir en línea y buscar

83
00:03:41,720 --> 00:03:45,890
Stop Rosebank y Stop Cambo. E incluso

84
00:03:45,890 --> 00:03:47,000
si no estás en las calles de

85
00:03:47,000 --> 00:03:49,430
Londres para protestar o en Escocia, tú

86
00:03:49,430 --> 00:03:52,010
puedes hacer mucho digitalmente desde

87
00:03:52,010 --> 00:03:54,609
tu ordenador en casa.

88
00:03:54,617 --> 00:03:56,537
Si no te importa terminaré con

89
00:03:56,540 --> 00:03:58,310
un canto rápido. Entonces, Milan, necesito

90
00:03:58,310 --> 00:04:01,190
¡tu ayuda aquí! Comencemos.

91
00:04:01,190 --> 00:04:03,740
Comprobación del micrófono: Así que si digo:

92
00:04:03,740 --> 00:04:06,650
"¡Para! Cuando digo: '¡Para! Tú dices:

93
00:04:06,650 --> 00:04:09,560
"¡Rosebank! Cuando digo: "¡Alto! Dices:

94
00:04:09,560 --> 00:04:13,520
"¡Rosebank! Hasta luego Milan. Muchas gracias bye bye.

95
00:04:14,250 --> 00:04:17,610
En resumen, Rosebank puede

96
00:04:17,640 --> 00:04:20,370
no ser explotado, porque queremos que sea

97
00:04:20,370 --> 00:04:23,789
simplemente no puede permitirse más en vista del calentamiento global

98
00:04:23,840 --> 00:04:26,090
también puede seguir transportando este CO2 al

99
00:04:26,090 --> 00:04:29,130
al aire. Pero Gran Bretaña, el gobierno

100
00:04:29,130 --> 00:04:32,520
de Gran Bretaña es incluso la promoción de este

101
00:04:32,520 --> 00:04:34,150
yacimiento de petróleo y gas. Además

102
00:04:34,200 --> 00:04:37,200
que Rosebank es en realidad una zona protegida

103
00:04:37,200 --> 00:04:40,440
región y es una

104
00:04:40,440 --> 00:04:44,190
corredor migratorio para las ballenas. Quien lo solicite

105
00:04:44,190 --> 00:04:46,680
quiere firmar las peticiones que mencionó Heather,

106
00:04:46,680 --> 00:04:52,440
en la página web 99-percent.org

107
00:04:52,440 --> 00:04:54,360
encontrará más información.

108
00:04:54,360 --> 00:04:58,320
No sólo Equinor y el

109
00:04:58,320 --> 00:05:02,010
Gobierno del Reino Unido quieren crear en el Mar del Norte

110
00:05:02,310 --> 00:05:05,760
petróleo, Shell también sigue con

111
00:05:05,760 --> 00:05:09,990
y Greenpeace ha protestado contra ella.

112
00:05:10,110 --> 00:05:13,350
y es que los activistas han

113
00:05:13,469 --> 00:05:14,520
plataforma de producción

114
00:05:14,520 --> 00:05:17,700
enganchada en el Mar del Norte

115
00:05:17,700 --> 00:05:21,087
fue transportado. A modo de antecedentes, Shell ha

116
00:05:21,087 --> 00:05:24,990
obtuvo un beneficio récord el año pasado,

117
00:05:24,990 --> 00:05:28,110
debido a la subida de los precios del petróleo tras la guerra de Ucrania.

118
00:05:28,200 --> 00:05:32,160
El beneficio ajustado se duplicó

119
00:05:32,490 --> 00:05:34,140
hasta los 36.000 millones de euros.

120
00:05:34,400 --> 00:05:37,520
Así que Shell es un ganador de guerra. Así son las cosas ahora,

121
00:05:37,520 --> 00:05:40,000
que la empresa siga invirtiendo en actividades que dañan el clima

122
00:05:39,980 --> 00:05:44,740
combustibles fósiles, ¡es una irresponsabilidad!

123
00:05:45,300 --> 00:05:48,539
Silja Zimmermann, activista escaladora de Greenpeace Alemania,

124
00:05:48,810 --> 00:05:51,210
estuvo presente en la acción de protesta en el Mar del Norte.

125
00:05:51,300 --> 00:05:54,360
Exijo a Shell este proyecto inmediatamente

126
00:05:54,360 --> 00:05:54,840
y que se retire de nuestro

127
00:05:55,440 --> 00:05:58,710
gobiernos por fin exijo responsabilidades

128
00:05:58,740 --> 00:06:01,860
y en su lugar invertir significativamente en

129
00:06:01,860 --> 00:06:05,700
energías renovables. Con esta plataforma de producción

130
00:06:05,700 --> 00:06:09,330
Shell planea 8 pozos más en el Mar del Norte

131
00:06:09,330 --> 00:06:13,380
Campo petrolífero Penguins. Es el primer

132
00:06:13,440 --> 00:06:14,789
nuevo tripulado

133
00:06:15,030 --> 00:06:17,760
Plataforma de producción de petróleo para Shell en el norte

134
00:06:17,760 --> 00:06:20,730
Mar del Norte durante 30 años. Este proyecto es

135
00:06:20,730 --> 00:06:22,330
fuera de plazo. Si ahora

136
00:06:22,320 --> 00:06:23,440
fuera de tiempo. Si ahora

137
00:06:23,310 --> 00:06:23,430
ya en Shell, todavía puedo

138
00:06:23,440 --> 00:06:24,810
Ya estoy en Shell, todavía puedo

139
00:06:24,810 --> 00:06:26,669
buenas noticias

140
00:06:27,270 --> 00:06:31,380
Shell tiene que pagar por la contaminación, 15 millones de euros.

141
00:06:31,409 --> 00:06:35,970
Cuatro granjeros nigerianos y sus comunidades

142
00:06:36,780 --> 00:06:39,720
la petrolera Shell tiene que pagar

143
00:06:39,750 --> 00:06:43,679
15 millones de euros en indemnizaciones. Derribados oleoductos de Shell

144
00:06:43,679 --> 00:06:46,350
habían contaminado sus tierras.

145
00:06:46,350 --> 00:06:47,400
La asociación de socios BUND,

146
00:06:47,429 --> 00:06:51,659
Amigos de la Tierra, apoyó a los afectados.

147
00:06:51,659 --> 00:06:54,450
Eso fue en 2008, Shell fue

148
00:06:54,900 --> 00:06:58,530
acusada en la sede de la compañía en La Haya.

149
00:06:58,530 --> 00:07:02,580
Mientras tanto, se ha llegado al veredicto, pero

150
00:07:02,580 --> 00:07:06,030
las petroleras han tomado más pueblos en el

151
00:07:06,030 --> 00:07:07,560
Delta del Níger

152
00:07:07,560 --> 00:07:09,570
privados de sus medios de subsistencia.

153
00:07:10,409 --> 00:07:13,860
Al menos la indemnización establece un nuevo estándar.

154
00:07:14,400 --> 00:07:17,515
Lo dice Donald Pols, de Amigos de la Tierra Europa Holanda.

155
00:07:17,520 --> 00:07:18,520
En el futuro será más fácil

156
00:07:18,520 --> 00:07:20,820
En el futuro será más fácil

157
00:07:20,820 --> 00:07:23,580
las empresas a rendir cuentas.

158
00:07:24,040 --> 00:07:26,860
Ahora hay que contrastar estos 15 millones

159
00:07:26,940 --> 00:07:31,830
con los 36 mil millones de beneficio,

160
00:07:31,830 --> 00:07:37,049
que Shell obtuvo el año pasado.

161
00:07:37,409 --> 00:07:40,320
Así que los órdenes de magnitud de los que estamos hablando,

162
00:07:40,320 --> 00:07:44,070
0,5 por ciento de los beneficios que Shell tiene que

163
00:07:44,669 --> 00:07:48,240
en concepto de indemnización.

164
00:07:48,240 --> 00:07:51,390
Así que sale de la caja chica se puede decir.

165
00:07:51,392 --> 00:07:53,400
No obstante, marquemos esto como una buena noticia.