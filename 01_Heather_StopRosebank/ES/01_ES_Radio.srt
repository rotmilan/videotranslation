1
00:00:00,090 --> 00:00:02,690
Este es el mostrador de información de Regeneradio,

2
00:00:02,719 --> 00:00:04,920
la radio Greenpeace,

3
00:00:04,930 --> 00:00:06,810
en la radio libre de Stuttgart.

4
00:00:06,890 --> 00:00:09,860
La protesta climática en Lützerath,

5
00:00:09,860 --> 00:00:13,820
tiene mucha atención internacional

6
00:00:13,820 --> 00:00:15,890
atención. Esa es también una razón

7
00:00:15,920 --> 00:00:19,840
por la que la manifestación del 11.02 en Stuttgart fue internacional.

8
00:00:19,880 --> 00:00:22,310
También vemos que la interconexión de la

9
00:00:22,310 --> 00:00:26,390
movimiento de protección del clima está progresando en todo el mundo.

10
00:00:26,390 --> 00:00:29,540
Así que no es de extrañar que

11
00:00:29,540 --> 00:00:32,750
tenga un discurso en inglés.

12
00:00:32,750 --> 00:00:37,370
Subimos a Heather de la organización 99-percent.org.

13
00:00:37,519 --> 00:00:39,919
La dirección web es www.99-percent.org.

14
00:00:39,980 --> 00:00:43,459
Es una organización con

15
00:00:43,459 --> 00:00:47,390
tiene su origen en el Reino Unido. Hace algo sobre

16
00:00:47,390 --> 00:00:53,000
nuestra economía actual bajo el capitalismo.

17
00:00:53,000 --> 00:00:55,790
Empobreciendo al 99 por ciento de la población

18
00:00:55,794 --> 00:00:58,849
y al 1 por ciento extremadamente más rico.

19
00:00:58,879 --> 00:01:00,080
Habla de su campaña actual.

20
00:01:00,110 --> 00:01:02,209
En primer lugar quiero decir que

21
00:01:02,209 --> 00:01:04,550
se trata de Rosebank y

22
00:01:04,580 --> 00:01:07,070
un yacimiento de petróleo y gas aún sin explotar

23
00:01:07,070 --> 00:01:09,920
en el Mar del Norte. La empresa Equinor quiere

24
00:01:10,040 --> 00:01:11,420
explotar eso ahora.