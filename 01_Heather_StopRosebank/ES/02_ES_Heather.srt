25
00:01:13,400 --> 00:01:17,120
Hola Stuttgart y gracias

26
00:01:17,120 --> 00:01:19,220
StrikeWEF y Milan en StrikeWEF por esto

27
00:01:19,220 --> 00:01:21,259
oportunidad de hablar. Mi nombre es Heather

28
00:01:21,380 --> 00:01:23,480
y hoy les hablo desde

29
00:01:23,480 --> 00:01:25,250
el Reino Unido. Estoy aquí para hablar

30
00:01:25,250 --> 00:01:27,530
sobre una campaña que se está llevando a cabo en el

31
00:01:27,530 --> 00:01:30,650
una campaña medioambiental muy importante llamada

32
00:01:30,920 --> 00:01:31,700
Stop Rosebank.

33
00:01:31,710 --> 00:01:33,750
Déjame hablarte de Rosebank.

34
00:01:33,760 --> 00:01:36,680
Rosebank es el mayor yacimiento de petróleo y gas sin explotar de

35
00:01:36,680 --> 00:01:38,570
el mar del norte y el mar del norte

36
00:01:38,570 --> 00:01:41,240
está al norte de escocia. Por qué

37
00:01:41,240 --> 00:01:43,070
¿es para tanto?

38
00:01:43,100 --> 00:01:44,900
Es un gran problema porque Rosebank es

39
00:01:44,990 --> 00:01:47,090
enorme. Es tres veces el tamaño de

40
00:01:47,090 --> 00:01:49,580
el yacimiento petrolífero de Cambo y puede que hayas oído

41
00:01:49,580 --> 00:01:51,770
de Cambo porque en el último par

42
00:01:51,770 --> 00:01:52,039
de años

43
00:01:52,039 --> 00:01:55,729
los activistas y manifestantes han conseguido

44
00:01:55,729 --> 00:02:00,140
conseguir que Shell se retire de Cambo. Así que

45
00:02:00,160 --> 00:02:03,080
si quemáramos todo el

46
00:02:03,080 --> 00:02:05,300
el petróleo y el gas de Rosebank,

47
00:02:05,320 --> 00:02:07,759
produciría tanto CO2 -

48
00:02:07,779 --> 00:02:10,699
más CO2 que los 28 más bajos

49
00:02:10,699 --> 00:02:12,200
ingresos del mundo.

50
00:02:12,200 --> 00:02:14,060
Así que permítanme decir que

51
00:02:14,120 --> 00:02:17,240
de nuevo - si quemamos todo el petróleo en Rosebank

52
00:02:17,240 --> 00:02:18,740
la cantidad de CO2 es

53
00:02:18,740 --> 00:02:21,380
equivalente a la producida por el

54
00:02:21,410 --> 00:02:23,780
700 millones de personas que viven en esos

55
00:02:23,780 --> 00:02:25,940
28 países.

56
00:02:26,850 --> 00:02:28,490
Entonces, ¿por qué te hablo de

57
00:02:28,490 --> 00:02:31,640
¿esto ahora? Porque es urgente.

58
00:02:31,640 --> 00:02:32,799
Una empresa llamada Equinor,

59
00:02:33,230 --> 00:02:37,670
que es la compañía petrolera estatal de Noruega, tiene

60
00:02:37,670 --> 00:02:39,670
solicitado al Gobierno del Reino Unido el desarrollo de Rosebank

61
00:02:39,691 --> 00:02:44,600
y la decisión es inminente por lo que

62
00:02:46,040 --> 00:02:47,630
es muy importante que todos apoyemos

63
00:02:47,630 --> 00:02:50,329
esta campaña. ¿Por qué es tan

64
00:02:50,329 --> 00:02:52,490
algo malo? ¿Sabes por qué es

65
00:02:52,490 --> 00:02:53,990
algo malo. Todos sabemos que el

66
00:02:53,990 --> 00:02:56,030
ciencia. Todos sabemos que

67
00:02:56,030 --> 00:02:58,670
tenemos que detener todo nuevo petróleo y

68
00:02:58,670 --> 00:03:01,550
proyectos de gas. Y es algo realmente malo

69
00:03:01,550 --> 00:03:03,110
también para la ecología.

70
00:03:03,110 --> 00:03:06,110
El yacimiento petrolífero de Rosebank está en el

71
00:03:06,130 --> 00:03:09,890
corredor migratorio de cachalotes y rorcuales comunes

72
00:03:09,960 --> 00:03:13,280
y también es un área ecológicamente protegida

73
00:03:13,270 --> 00:03:17,510
debido al cinturón de esponjas Shetland-Faroe

74
00:03:17,540 --> 00:03:20,420
También el gobierno del Reino Unido

75
00:03:20,420 --> 00:03:22,609
absolutamente cómplice de esto porque son

76
00:03:22,790 --> 00:03:26,120
subvencionando esta exploración petrolífera

77
00:03:26,120 --> 00:03:28,820
masivamente por valor de

78
00:03:28,850 --> 00:03:31,530
500 millones de libras de los contribuyentes británicos

79
00:03:31,560 --> 00:03:34,000
que no verán nada de ello.

80
00:03:33,990 --> 00:03:36,590
De todos modos, ¿qué puedes hacer para ayudar?

81
00:03:36,590 --> 00:03:38,090
Mi llamada a la acción para ti

82
00:03:38,090 --> 00:03:41,720
hoy es esta. Es ir en línea y buscar

83
00:03:41,720 --> 00:03:45,890
Stop Rosebank y Stop Cambo. E incluso

84
00:03:45,890 --> 00:03:47,000
si no estás en las calles de

85
00:03:47,000 --> 00:03:49,430
Londres para protestar o en Escocia, tú

86
00:03:49,430 --> 00:03:52,010
puedes hacer mucho digitalmente desde

87
00:03:52,010 --> 00:03:54,609
tu ordenador en casa.

88
00:03:54,617 --> 00:03:56,537
Si no te importa terminaré con

89
00:03:56,540 --> 00:03:58,310
un canto rápido. Entonces, Milan, necesito

90
00:03:58,310 --> 00:04:01,190
¡tu ayuda aquí! Comencemos.

91
00:04:01,190 --> 00:04:03,740
Comprobación del micrófono: Así que si digo:

92
00:04:03,740 --> 00:04:06,650
"¡Para! Cuando digo: '¡Para! Tú dices:

93
00:04:06,650 --> 00:04:09,560
"¡Rosebank! Cuando digo: "¡Alto! Dices:

94
00:04:09,560 --> 00:04:13,520
"¡Rosebank! Hasta luego Milan. Muchas gracias bye bye.