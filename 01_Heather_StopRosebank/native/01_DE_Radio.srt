1
00:00:00,090 --> 00:00:02,690
Hier ist die Inforredaktion Regeneradio,

2
00:00:02,719 --> 00:00:04,920
das Greenpeace-Radio,

3
00:00:04,930 --> 00:00:06,810
im freien Radio für Stuttgart.

4
00:00:06,890 --> 00:00:09,860
Der Klimaprotest in Lützerath,

5
00:00:09,860 --> 00:00:13,820
hat sehr viel internationale Aufmerksamkeit

6
00:00:13,820 --> 00:00:15,890
auf sich gezogen. Das ist auch ein Grund

7
00:00:15,920 --> 00:00:19,840
warum die Demo, am 11.02 in Stuttgart, international besetzt war.

8
00:00:19,880 --> 00:00:22,310
Wir sehen auch, dass die Vernetzung der

9
00:00:22,310 --> 00:00:26,390
Klimaschutzbewegung weltweit voranschreitet.

10
00:00:26,390 --> 00:00:29,540
So ist es auch nicht überraschend, dass ich noch

11
00:00:29,540 --> 00:00:32,750
einen englischsprachigen Redebeitrag bekommen habe.

12
00:00:32,750 --> 00:00:37,370
Wir höheren Heather von der Organisation 99-percent.org.

13
00:00:37,519 --> 00:00:39,919
Die Web-adresse ist www.99-percent.org.

14
00:00:39,980 --> 00:00:43,459
Es ist eine Organisation mit

15
00:00:43,459 --> 00:00:47,390
Ursprung in Großbritannien. Sie tut was gegen

16
00:00:47,390 --> 00:00:53,000
unser heutiges Wirtschaften im Kapitalismus.

17
00:00:53,000 --> 00:00:55,790
Das 99 Prozent der Bevölkerung ärmer macht

18
00:00:55,794 --> 00:00:58,849
und 1 Prozent extrem viel reicher.

19
00:00:58,879 --> 00:01:00,080
Sie spricht über ihre aktuelle Kampagne.

20
00:01:00,110 --> 00:01:02,209
Vorab will ich noch sagen, dass es

21
00:01:02,209 --> 00:01:04,550
hier um Rosebank geht und

22
00:01:04,580 --> 00:01:07,070
ein noch nicht genutztes Öl- und Gasfeld

23
00:01:07,070 --> 00:01:09,920
in der Nordsee. Die Firma Equinor will

24
00:01:10,040 --> 00:01:11,420
das jetzt ausbeuten.