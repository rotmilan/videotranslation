95
00:04:14,250 --> 00:04:17,610
Auf deutsch kurz zusammengefasst, Rosebank darf

96
00:04:17,640 --> 00:04:20,370
nicht ausgebeutet werden, weil wir es uns

97
00:04:20,370 --> 00:04:23,789
angesichts der Klimaerwärmung einfach nicht mehr leisten

98
00:04:23,840 --> 00:04:26,090
können dieses CO2 auch noch in die

99
00:04:26,090 --> 00:04:29,130
Luft zu blasen. Aber Großbritannien, die Regierung

100
00:04:29,130 --> 00:04:32,520
von Großbritannien fördert sogar noch dieses

101
00:04:32,520 --> 00:04:34,150
Öl- und Gas-Feld. Dazu kommt

102
00:04:34,200 --> 00:04:37,200
noch das Rosebank eigentlich eine geschützte

103
00:04:37,200 --> 00:04:40,440
Region ist und es ist ein

104
00:04:40,440 --> 00:04:44,190
Wanderkorridor für Wale. Wer die Petitionen

105
00:04:44,190 --> 00:04:46,680
unterschreiben will, die Heather genannt hat,

106
00:04:46,680 --> 00:04:52,440
auf der Internetseite 99-percent.org

107
00:04:52,440 --> 00:04:54,360
kommt man auf alle weiteren Infos dazu.

108
00:04:54,360 --> 00:04:58,320
Nicht nur Equinor und die

109
00:04:58,320 --> 00:05:02,010
Regierung von Großbritannien wollen in der Nordsee

110
00:05:02,310 --> 00:05:05,760
Öl schürfen, auch Shell ist noch mit

111
00:05:05,760 --> 00:05:09,990
dabei und da hat Greenpeace dagegen protestiert

112
00:05:10,110 --> 00:05:13,350
und zwar haben sich Aktivisten an die

113
00:05:13,469 --> 00:05:14,520
Förderplattform

114
00:05:14,520 --> 00:05:17,700
gehängt die in die Nordsee rein

115
00:05:17,700 --> 00:05:21,087
transportiert wurde. Zum Hintergrund, Shell hat

116
00:05:21,087 --> 00:05:24,990
im vergangenen Jahr einen Rekordgewinn erzielt,

117
00:05:24,990 --> 00:05:28,110
wegen der gestiegenen Ölpreise im Zuge des Ukraine-Krieg.

118
00:05:28,200 --> 00:05:32,160
Der bereinigte Gewinn hat sich verdoppelt

119
00:05:32,490 --> 00:05:34,140
auf 36 Milliarden Euro.

120
00:05:34,400 --> 00:05:37,520
Shell ist also Kriegsgewinner. Ist jetzt halt so aber,

121
00:05:37,520 --> 00:05:40,000
dass der Konzern dann trotzdem noch in klimaschädliche

122
00:05:39,980 --> 00:05:44,740
Fossile Brennstoffe investiert ist unverantwortlich!

123
00:05:45,300 --> 00:05:48,539
Silja Zimmermann, Kletteraktivistin von Greenpeace Deutschland,

124
00:05:48,810 --> 00:05:51,210
war bei der Protestaktion in der Nordsee dabei.

125
00:05:51,300 --> 00:05:54,360
Ich fordere Shell dieses Vorhaben sofort

126
00:05:54,360 --> 00:05:54,840
zu beenden und von unseren

127
00:05:55,440 --> 00:05:58,710
Regierungen fordere ich endlich Verantwortung

128
00:05:58,740 --> 00:06:01,860
zu übernehmen und stattdessen signifikante Investition in

129
00:06:01,860 --> 00:06:05,700
Erneuerbare Energien zu stecken. Mit dieser Produktionsplattform

130
00:06:05,700 --> 00:06:09,330
plant Shell 8 weitere Bohrlöcher in Nordsee

131
00:06:09,330 --> 00:06:13,380
Ölfeld Penguins auszubeuten. Es ist die erste

132
00:06:13,440 --> 00:06:14,789
neue bemannte

133
00:06:15,030 --> 00:06:17,760
Ölförderplattform für Shell in der nördlichen

134
00:06:17,760 --> 00:06:20,730
Nordsee seit 30 Jahren. Dieses Projekt ist

135
00:06:20,730 --> 00:06:22,330
aus der Zeit gefallen. Wenn ich jetzt

136
00:06:22,320 --> 00:06:23,440
aus der Zeit gefallen. Wenn ich jetzt

137
00:06:23,310 --> 00:06:23,430
schon bei Shell bin, kann ich noch

138
00:06:23,440 --> 00:06:24,810
schon bei Shell bin, kann ich noch

139
00:06:24,810 --> 00:06:26,669
eine gute Nachricht verkünden

140
00:06:27,270 --> 00:06:31,380
Shell muss für Verschmutzung zahlen, 15 Millionen Euro.

141
00:06:31,409 --> 00:06:35,970
Vier nigerianische Bauern und ihre Gemeinden

142
00:06:36,780 --> 00:06:39,720
muss der Ölkonzern Shell mit

143
00:06:39,750 --> 00:06:43,679
15 Millionen Euro entschädigen. Weggeschlagene Shell-Pipelines

144
00:06:43,679 --> 00:06:46,350
hatten ihr Land verseucht.

145
00:06:46,350 --> 00:06:47,400
Der BUND Partnerverein,

146
00:06:47,429 --> 00:06:51,659
Friends of the Earth, hat die Betroffenen unterstützt.

147
00:06:51,659 --> 00:06:54,450
Das war im Jahr 2008, da wurde Shell

148
00:06:54,900 --> 00:06:58,530
angeklagt am Hauptsitz des Konzerns in den Den Haag.

149
00:06:58,530 --> 00:07:02,580
Inzwischen ist das Urteil gefällt, allerdings

150
00:07:02,580 --> 00:07:06,030
haben die Öl Konzerne weitere Dörfer im

151
00:07:06,030 --> 00:07:07,560
Nigerdelta

152
00:07:07,560 --> 00:07:09,570
ihrer Lebensgrundlage beraubt.

153
00:07:10,409 --> 00:07:13,860
Immerhin setzt die Entschädigung einen neuen Maßstab.

154
00:07:14,400 --> 00:07:17,515
Sagt Donald Pols von Friends of the Earth Europe Niederlande.

155
00:07:17,520 --> 00:07:18,520
Es ist zukünftig einfacher die

156
00:07:18,520 --> 00:07:20,820
Es ist zukünftig einfacher die

157
00:07:20,820 --> 00:07:23,580
Konzerne zur Rechenschaft zu ziehen.

158
00:07:24,040 --> 00:07:26,860
Jetzt muss man diese 15 Millionen gegenüberstellen

159
00:07:26,940 --> 00:07:31,830
zu den 36 Milliarden Gewinn,

160
00:07:31,830 --> 00:07:37,049
den Shell letztes Jahr erwirtschaftet hat.

161
00:07:37,409 --> 00:07:40,320
Also die Grössenordnungen um die es da geht,

162
00:07:40,320 --> 00:07:44,070
0,5 Prozent des Gewinns muss Shell

163
00:07:44,669 --> 00:07:48,240
an Entschädigung zahlen.

164
00:07:48,240 --> 00:07:51,390
Geht also aus der Portokasse kann gesagt werden.

165
00:07:51,392 --> 00:07:53,400
Trotzdem haken wir das jetzt mal als gute Nachricht ab.

