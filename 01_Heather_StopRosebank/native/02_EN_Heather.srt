25
00:01:13,400 --> 00:01:17,120
Hello Stuttgart and thank you

26
00:01:17,120 --> 00:01:19,220
StrikeWEF and Milan at StrikeWEF for this

27
00:01:19,220 --> 00:01:21,259
opportunity to talk. My name is Heather

28
00:01:21,380 --> 00:01:23,480
and I'm speaking to you today from

29
00:01:23,480 --> 00:01:25,250
the UK. I'm here to talk

30
00:01:25,250 --> 00:01:27,530
about a campaign that's running at the

31
00:01:27,530 --> 00:01:30,650
moment, a really important environmental campaign called

32
00:01:30,920 --> 00:01:31,700
Stop Rosebank.

33
00:01:31,710 --> 00:01:33,750
Let me tell you about Rosebank.

34
00:01:33,760 --> 00:01:36,680
Rosebank is the biggest undeveloped oil and gasfield in

35
00:01:36,680 --> 00:01:38,570
the north sea and the north sea

36
00:01:38,570 --> 00:01:41,240
is to the north of scotland. Why

37
00:01:41,240 --> 00:01:43,070
is it such a big deal?

38
00:01:43,100 --> 00:01:44,900
It's a big deal because Rosebank is

39
00:01:44,990 --> 00:01:47,090
enormous. It's three times the size of

40
00:01:47,090 --> 00:01:49,580
the Cambo oilfield and you might have heard

41
00:01:49,580 --> 00:01:51,770
of Cambo because over the last couple

42
00:01:51,770 --> 00:01:52,039
of years

43
00:01:52,039 --> 00:01:55,729
campaigners and protesters have managed

44
00:01:55,729 --> 00:02:00,140
to get Shell to withdraw from Cambo. So

45
00:02:00,160 --> 00:02:03,080
if we were to burn all the

46
00:02:03,080 --> 00:02:05,300
oil and gas from Rosebank,

47
00:02:05,320 --> 00:02:07,759
it would produce so much CO2 -

48
00:02:07,779 --> 00:02:10,699
more CO2 then the 28 lowest

49
00:02:10,699 --> 00:02:12,200
income countries in the world.

50
00:02:12,200 --> 00:02:14,060
So let me just say that

51
00:02:14,120 --> 00:02:17,240
again - if we burn all the oil in Rosebank

52
00:02:17,240 --> 00:02:18,740
the amount of CO2 is

53
00:02:18,740 --> 00:02:21,380
equivalent to that produced by the

54
00:02:21,410 --> 00:02:23,780
700 million people who live in those

55
00:02:23,780 --> 00:02:25,940
28 countries.

56
00:02:26,850 --> 00:02:28,490
So why am i telling you about

57
00:02:28,490 --> 00:02:31,640
this now? It's because it's urgent.

58
00:02:31,640 --> 00:02:32,799
A company called Equinor,

59
00:02:33,230 --> 00:02:37,670
which is Norway's state oil company, has

60
00:02:37,670 --> 00:02:39,670
applied to the UK Government to develop Rosebank

61
00:02:39,691 --> 00:02:44,600
and the decision is imminent so

62
00:02:46,040 --> 00:02:47,630
it's really important we all get behind

63
00:02:47,630 --> 00:02:50,329
this campaign. So why is it such

64
00:02:50,329 --> 00:02:52,490
a bad thing? You know why it's

65
00:02:52,490 --> 00:02:53,990
a bad thing. We all know the

66
00:02:53,990 --> 00:02:56,030
science. We all know that we just

67
00:02:56,030 --> 00:02:58,670
have to stop all new oil and

68
00:02:58,670 --> 00:03:01,550
gas projects. And it's a really bad thing

69
00:03:01,550 --> 00:03:03,110
for ecology too.

70
00:03:03,110 --> 00:03:06,110
The Rosebank oil field is in the

71
00:03:06,130 --> 00:03:09,890
migration corridor for sperm and fin whales

72
00:03:09,960 --> 00:03:13,280
and it's also an ecologically protected area

73
00:03:13,270 --> 00:03:17,510
because of the Shetland-Faroe Sponge Belt

74
00:03:17,540 --> 00:03:20,420
Also the UK government is

75
00:03:20,420 --> 00:03:22,609
absolutely complicit in this because they are

76
00:03:22,790 --> 00:03:26,120
subsidizing this oil field exploration

77
00:03:26,120 --> 00:03:28,820
massively to the tune of

78
00:03:28,850 --> 00:03:31,530
half a billion quid from UK taxpayers'

79
00:03:31,560 --> 00:03:34,000
who will see nothing from it.

80
00:03:33,990 --> 00:03:36,590
Anyway, what can you do to help?

81
00:03:36,590 --> 00:03:38,090
My call to action to you

82
00:03:38,090 --> 00:03:41,720
today is this. It is to go online and look

83
00:03:41,720 --> 00:03:45,890
up Stop Rosebank and Stop Cambo. And even

84
00:03:45,890 --> 00:03:47,000
if you're not on the streets of

85
00:03:47,000 --> 00:03:49,430
London to protest or in Scotland, you

86
00:03:49,430 --> 00:03:52,010
can do a great deal digitally from

87
00:03:52,010 --> 00:03:54,609
your computer at home.

88
00:03:54,617 --> 00:03:56,537
If you don't mind I'll finish with

89
00:03:56,540 --> 00:03:58,310
a quick chant. So, Milan, I need

90
00:03:58,310 --> 00:04:01,190
your help here! Let's begin.

91
00:04:01,190 --> 00:04:03,740
Mic check: So if i say:

92
00:04:03,740 --> 00:04:06,650
'Stop!' When i say: 'Stop!' You say:

93
00:04:06,650 --> 00:04:09,560
'Rosebank!' When i say: 'Stop!' You say:

94
00:04:09,560 --> 00:04:13,520
'Rosebank!' Over to you Milan. Many thanks bye bye.