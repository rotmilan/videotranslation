29
00:01:36,000 --> 00:01:37,979
hi everyone this is a message from

30
00:01:37,979 --> 00:01:40,530
the village of lizard in germany where

31
00:01:40,560 --> 00:01:42,030
after two and a half years of

32
00:01:42,030 --> 00:01:45,119
resistance and occupation we have been evicted

33
00:01:45,360 --> 00:01:47,190
by the german police forces a few

34
00:01:47,190 --> 00:01:47,699
days ago

35
00:01:48,960 --> 00:01:50,940
it has been an incredible struggle and

36
00:01:50,940 --> 00:01:53,699
incredible battle and an incredibly empowering one

37
00:01:53,699 --> 00:01:55,770
for all our movements and everyone involved

38
00:01:56,910 --> 00:01:59,100
this village has been not only a

39
00:01:59,100 --> 00:02:01,740
symbol but a very concrete redline for

40
00:02:01,770 --> 00:02:03,660
the german climate movement and the german

41
00:02:03,660 --> 00:02:06,690
government by deciding to evict the people

42
00:02:06,690 --> 00:02:08,370
who have been living in this village

43
00:02:08,370 --> 00:02:09,810
for two and a half years the

44
00:02:09,810 --> 00:02:11,700
government in germany has shown that it

45
00:02:11,700 --> 00:02:13,470
has no respect for the paris agreement

46
00:02:13,500 --> 00:02:15,090
so it has no respect for lives

47
00:02:15,090 --> 00:02:17,040
that it has no respect for sestina

48
00:02:17,040 --> 00:02:18,840
nbl future and for the rights of

49
00:02:18,840 --> 00:02:21,570
peoples throughout the world the struggle we

50
00:02:21,570 --> 00:02:23,850
haven't melted on our own we we

51
00:02:23,850 --> 00:02:26,459
built it on the shoulders of giants

52
00:02:26,519 --> 00:02:28,110
of giants across the world who have

53
00:02:28,110 --> 00:02:31,440
been resisting a neocolonial the colonial and

54
00:02:31,890 --> 00:02:35,280
tropical and a capitalist system for so

55
00:02:35,280 --> 00:02:37,200
many hundreds of years we

56
00:02:37,200 --> 00:02:38,730
we are very new you know movements

57
00:02:38,730 --> 00:02:40,500
compared to those people from whom we

58
00:02:40,500 --> 00:02:42,329
are learning every year in every one

59
00:02:42,329 --> 00:02:44,790
of our struggles and we've been fighting

60
00:02:44,910 --> 00:02:46,500
the best of our abilities and we

61
00:02:46,500 --> 00:02:48,959
are still fighting in the neighbouring village

62
00:02:48,959 --> 00:02:50,910
of kinberg or canbus just standing and

63
00:02:50,910 --> 00:02:52,890
people are still going inaction every day

64
00:02:54,120 --> 00:02:55,530
and i just want to say a

65
00:02:55,530 --> 00:02:56,970
huge thank you to everyone who is

66
00:02:56,970 --> 00:02:59,280
taking action today you know studied already

67
00:02:59,310 --> 00:03:01,200
gives us hope and we do know

68
00:03:01,200 --> 00:03:03,690
that none of the struggle is has

69
00:03:03,690 --> 00:03:06,540
just one face right it doesn't matter

70
00:03:06,600 --> 00:03:07,589
if you're standing on the edge of

71
00:03:07,589 --> 00:03:09,030
the coal mine or if you're actually

72
00:03:09,030 --> 00:03:11,280
fair fighting the ones who are financing

73
00:03:11,280 --> 00:03:13,500
the exploitation of this coal mine out

74
00:03:13,500 --> 00:03:14,130
w e

75
00:03:14,790 --> 00:03:17,820
realize are millions of investment being financed

76
00:03:17,820 --> 00:03:20,130
by banks by different financial actors like

77
00:03:20,130 --> 00:03:23,730
picked it like credit suisse like all

78
00:03:23,730 --> 00:03:24,899
the ones that you know all too

79
00:03:24,899 --> 00:03:27,899
well those ones they are responsible for

80
00:03:27,899 --> 00:03:30,750
what happened they are responsible for giving

81
00:03:30,780 --> 00:03:33,899
the lifeline tuesday exploitation tools is destruction

82
00:03:34,170 --> 00:03:34,320
there

83
00:03:34,350 --> 00:03:36,630
have to pull out it their responsibility

84
00:03:36,780 --> 00:03:39,329
it's our duty otherwise not only as

85
00:03:39,329 --> 00:03:42,089
a complicit to an incredible climate and

86
00:03:42,089 --> 00:03:42,810
human crime

87
00:03:43,980 --> 00:03:45,450
but they're actually the ones who are

88
00:03:45,450 --> 00:03:48,209
making it possible they're not just complicit

89
00:03:48,450 --> 00:03:50,100
they're the one making it possible

90
00:03:51,570 --> 00:03:53,519
so today i just want to send

91
00:03:53,519 --> 00:03:55,019
a quick message to all of you

92
00:03:55,050 --> 00:03:57,839
who aren't there and to who whichever

93
00:03:57,930 --> 00:04:00,839
office of whichever financial actor you're standing

94
00:04:00,839 --> 00:04:03,810
in front of thank you to everyone

95
00:04:03,810 --> 00:04:05,909
of you standing here today thank you

96
00:04:05,909 --> 00:04:08,850
for acting in solidarity and to whoever

97
00:04:08,969 --> 00:04:11,279
you're chanting at today be

98
00:04:11,279 --> 00:04:13,890
afraid we're here we're not going anywhere

99
00:04:14,100 --> 00:04:15,269
and we are going to take you

100
00:04:15,300 --> 00:04:17,880
down because this is not a game

101
00:04:18,450 --> 00:04:20,130
our lives the lives of people throughout

102
00:04:20,130 --> 00:04:22,350
the world the ability for ecosystem to

103
00:04:22,350 --> 00:04:24,480
regenerate and sustain life is not a

104
00:04:24,480 --> 00:04:26,490
game and we are not playing we

105
00:04:26,490 --> 00:04:28,260
are fighting we are angry we are

106
00:04:28,260 --> 00:04:30,719
fighting and we will keep fighting thank

107
00:04:30,719 --> 00:04:31,650
you so much